f  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo isset($url_back) ? $url_back : NULL; ?>">Transaksi</a>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-ha  spopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="<?= base_url('assets/img/theme/team-4-800x800.jpg') ?>">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold"><?= ucwords($this->session->userdata('username')) ?></span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <a href="<?= base_url('logout') ?>" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">    
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col-xl-8 mb-5 mb-xl-0">       
        </div>
      </div>
      <div class="row mt-5">
        <div class="col mb-5 mb-xl-0">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-3"><?= $title ?> Transaksi</h3>
                </div>
                <div class="col-12 my-3 form-1">
                  <form action="<?= base_url($action) ?>" method="post">
                  <?php 
                    if(!empty($id)):
                  ?>
                  <input type="hidden" name="id" value="<?= $id ?>">
                  <?php endif; ?>
                  <div class="row mb-4">
                    <div class="col-4">
                      <label for="kode">Kode Transaksi</label>
                      <input type="text" name="kode" value="<?= $kode?>" class="form-control" readonly>
                    </div>
                    <div class="col-4">
                        <label for="datepicker">Tanggal</label>
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div>
                            <input class="form-control" id="datepicker" name="tgl_transaksi" type="text" value="<?php echo isset( $data_transaksi['transaksi']->tgl_transaksi) ? $data_transaksi['transaksi']->tgl_transaksi : NULL ?>">
                        </div>
                        <?= form_error('tgl_transaksi') ?>
                    </div>
                  </div>
                  <div class="row mb-4">
                    <div class="col-4">
                      <label for="nama_reff">Nama Akun</label>
                      <select class="form-control" id="nama_reff" name="nama_reff" onchange="getNamaReff(this);">
                        <option value="null">Pilih Akun</option>
                      <?php if(isset($akun)){
                        foreach ($akun as $key => $value) {
                          # code...
                          if(isset($data_transaksi['transaksi']->no_reff)){
                            if($data_transaksi['transaksi']->no_reff==$value->no_reff){
                              echo '<option selected value="'.$value->no_reff.'">'.$value->nama_reff.'</option>';
                            }
                            else{
                              echo '<option value="'.$value->no_reff.'">'.$value->nama_reff.'</option>';
                            }
                          }
                          else{
                            echo '<option value="'.$value->no_reff.'">'.$value->nama_reff.'</option>';
                          }
                        }
                      }?>
                      </select>
                    </div>
                    <div class="col-4">
                        <label for="no_reff">No Reff</label>
                        <input type="text" name="no_reff" id="no_reff" class="form-control" value="<?php echo isset($data_transaksi['transaksi']->no_reff) ? $data_transaksi['transaksi']->no_reff : NULL ?>" readonly>
                    </div>
                    <div class="col-4">
                        <label for="memo">Memo</label>
                        <textarea class="form-control" name="memo" id="memo"><?php echo isset($data_transaksi['transaksi']->memo) ? $data_transaksi['transaksi']->memo : NULL ?></textarea>
                    </div>
                  </div>
                  <div class="row mb-4">
                  <table id="table-transaksi">
                    <thead>
                      <tr><td><div class="col">Deskripsi Transaksi</div></td>
                        <td><div class="col">Jenis Saldo</div></td>
                        <td><div class="col">Saldo</div></td>
                        <td><div class="col">Action</div></td></tr>
                    </thead>
                    <tbody>
                      <?php if($data_transaksi['transaksi_item']->num_rows()>0){
                        foreach ($data_transaksi['transaksi_item']->result_array() as $key => $value) {
                          if($value['jenis_saldo']=='debit'){
                            $dropDown = '<option value="debit" selected>Debit</option>'.
                                    '<option value="kredit">Kredit</option>';
                          } else{
                            $dropDown = '<option value="debit">Debit</option>'.
                                    '<option value="kredit" selected>Kredit</option>';
                          }
                          echo '<tr id="row-'.$key.'"><td style="display:none;">'.
                                '<div class="col">'.
                                  '<input type="text" readonly class="form-control" name="id[]" id="id-"'.$key.' value="'.$value['id'].'">'.
                              '</div>'.
                              '</td><td>'.
                                '<div class="col">'.
                                  '<input type="text" class="form-control" name="deskripsi[]" id="deskripsi-"'.$key.' value="'.$value['deskripsi'].'">'.
                              '</div>'.
                              '</td>'.
                              '<td>'.
                                '<div class="col">'.
                                  '<select class="form-control" id="jenis_saldo" name="jenis_saldo[]">'.
                                    $dropDown.
                                  '</select>'.
                              '</div></td>'.
                              '<td>'.
                                '<div class="col">'.
                                  '<input type="number" name="saldo[]" class="form-control saldo" id="saldo" value="'.$value['saldo'].'">'.
                              '</div></td>
                              <td><div class="col"><button type="button" class="form-control btn btn-danger" onclick="deleteTransaksiItem(this)" data-id="'.$value['id'].'">Delete</button></div></td>';
                        }
                      }?>
                    </tbody>
                  </table>
                  </div>
                  <div class="col-12" id="form_jurnal_prepend">
                    <button class="btn btn-primary" type="submit" id="button_jurnal">Simpan</button>
                    <button class="btn btn-warning" type="button" id="btn_add_row">Tambah</button>
                  </div> 
                  </form> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      