<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller{

	 public function __construct(){
        parent::__construct();
        $this->load->helper(['url','form','sia','tgl_indo']);
        $this->load->library(['session','form_validation']);
        $this->load->model('Akun_model','akun',true);
        $this->load->model('Transaksi_model','transaksi',true);
        $this->load->model('User_model','user',true);
        $login = $this->session->userdata('login');
        if(!$login){
            redirect('login');
        }
        else{
            if($this->session->userdata('level')!='admin'){
               redirect('error_web/privilages_user');
            }
        }
    }
    public function index(){
    	$titleTag = 'Transaksi';
        $content = 'transaksi/transaksi_main';
        $url_back = base_url('dashboard');
        $listTransaksi = $this->transaksi->getTransaksiByYearAndMonth()->result();
        $tahun = $this->transaksi->getTransaksiByYear()->result();
        $this->load->view('template',compact('content','listTransaksi','titleTag','tahun','url_back'));
    }
    public function add(){
        $title = 'Tambah'; 
        $content = 'transaksi/form_transaksi'; 
        $action = 'transaksi/add'; 
        $tgl_input = date('Y-m-d H:i:s'); 
        $id_user = $this->session->userdata('id');
        $titleTag = 'Tambah Transaksi';
        $url_back = base_url('transaksi');
        $akun = $this->akun->getAkun();
        if(!$_POST){
            $kode_transaksi = $this->transaksi->getNumTransaksi();
            $data = (object) $this->transaksi->getDefaultValues();
        }else{
            $transaksi = (object)[
                'id_user'=>$id_user,
                'kode'=>$this->input->post('kode',true),
                'no_reff'=>$this->input->post('no_reff',true),
                'memo'=>$this->input->post('memo',true),
                'tgl_input'=>$tgl_input,
                'tgl_transaksi'=>$this->input->post('tgl_transaksi',true),
            ];
            $length = count($_POST['deskripsi']);
            $transaksi_item = array();
            for($i=0; $i<$length; $i++) {
                $transaksi_item[$i] = [
                    'kode'=>$this->input->post('kode',true),
                    'saldo'=>$this->input->post('saldo')[$i],
                    'deskripsi'=>$this->input->post('deskripsi')[$i],
                    'jenis_saldo'=>$this->input->post('jenis_saldo')[$i],
                    'tgl_transaksi'=>$this->input->post('tgl_transaksi',true),
                    'waktu_input'=>$tgl_input,
                ];  
            }
        }

        if(!$this->transaksi->validate()){
            $this->load->view('template',compact('content','title','action','data','titleTag','kode_transaksi','akun'));
            return;
        }
        
        $this->transaksi->addTransaksi($transaksi,$transaksi_item);
        $this->session->set_flashdata('berhasil','Data Transaksi Berhasil Di Tambahkan');
        redirect('transaksi');    
    }
    public function detail(){
        $content = 'transaksi/transaksi';
        $titleTag = 'Transaksi';

        $url_back = base_url('transaksi');
        $kode = $this->input->post('kode',true);
        $transaksi = null;

        if(empty($kode)){
            redirect('transaksi');
        }

        $transaksi = $this->transaksi->getTransaksiJoinAkunDetail($kode);
        $totalDebit = $this->transaksi->getTotalSaldoDetail('debit',$kode);
        $totalKredit = $this->transaksi->getTotalSaldoDetail('kredit',$kode);

        if($transaksi==null){
            $this->session->set_flashdata('dataNull','Data Transaksi Dengan Kode '.$kode.' Tidak Di Temukan');
            redirect('transaksi');
        }

        $this->load->view('template',compact('content','transaksi','kode','totalDebit','totalKredit','titleTag','url_back'));
    }
    public function edit_form($kode){
        if($kode){
            $title = 'Edit'; 
            $content = 'transaksi/form_transaksi'; 
            $action = 'transaksi/edit'; 
            $titleTag = 'Edit Transaksi';
            $url_back = base_url('transaksi');
            $data_transaksi = $this->transaksi->viewTransaksi(['kode'=>$kode]);
            $akun = $this->akun->getAkun();
            $this->load->view('template',compact('content','url_back','title','akun','action','data_transaksi','kode','titleTag'));
        }else{
            redirect('transaksi');
        }
    }
    public function edit(){
        $title = 'Edit'; 
        $content = 'transaksi/form_transaksi'; 
        $action = 'transaksi/edit'; 
        $tgl_input = date('Y-m-d H:i:s'); 
        $id_user = $this->session->userdata('id'); $titleTag = 'Edit Transaksi';
        $url_back = base_url('transaksi/edit_form');

        if($_POST){
            $transaksi = (object)[
                'id_user'=>$id_user,
                'no_reff'=>$this->input->post('no_reff',true),
                'memo'=>$this->input->post('memo',true),
                'tgl_input'=>$tgl_input,
                'tgl_transaksi'=>$this->input->post('tgl_transaksi',true),
            ];
            $length = count($_POST['deskripsi']);
            $transaksi_item = array();
            for($i=0; $i<$length; $i++) {
                array_push($transaksi_item, array(
                    'id'=>isset($this->input->post('id')[$i]) ? $this->input->post('id')[$i] : NULL,
                    'kode' => $this->input->post('kode',true),  
                    'saldo'=>$this->input->post('saldo')[$i],
                    'deskripsi'=>$this->input->post('deskripsi')[$i],
                    'jenis_saldo'=>$this->input->post('jenis_saldo')[$i],
                    'tgl_transaksi'=>$this->input->post('tgl_transaksi',true),
                    'waktu_input'=>$tgl_input,
                ));  
            }
            $kode = $this->input->post('kode',true);
        }
        if(!$this->transaksi->validate()){
            $this->load->view('template',compact('content','title','action','data','id','titleTag'));
            return;
        }
        $this->transaksi->editTransaksi(['kode'=>$kode],$transaksi,$transaksi_item);
        $this->session->set_flashdata('berhasil','Data Transaksi Berhasil Di Ubah');
        redirect('transaksi');   
    }
    public function delete(){
        $kode = $this->input->post('kode',true);
        if(isset($kode)){
            $this->transaksi->removeTransaksi(['kode'=>$kode]);
            echo json_encode(['success'=>true]);
        }
        else{
            echo json_encode(['success'=>false]);
        }
    }
    public function deleteTransaksiItem(){
        $id = $this->input->post('id',true);
        if(isset($id)){
            $this->transaksi->deleteTransaksiItem(['id'=>$id]);
            echo json_encode(['success'=>true]);
        }
        else{
            echo json_encode(['success'=>false]);
        }
    }
}