<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_model extends CI_Model{

	public function getAbout($where=null){
		if($where){
			$this->db->where($where);
			return $this->db->get('about');
		}
		return $this->db->get('about');
	}
	public function updateAbout($where=null,$data){
		if($where){
			$this->db->where($where);
			return $this->db->update('about',$data);
		}
		return $this->db->update('about',$data);
	}
	public function deleteAbout($where = null){
		$this->db->where($where);
		return $this->db->delete('about');
	}
	public function getDefaultAbout(){
		return [
			'nama_instansi'=>'',
			'email'=>'',
			'no_telpon'=>'',
			'alamat'=>'',
			'foto'=>''
		];
	}
	public function aboutRules(){
		return [
			[
				'field'=>'nama_instansi',
				'label'=>'Nama Instansi',
				'rules'=>'trim|required',
			],
			[
				'field'=>'email',
				'rules'=>'trim|required',
				'label'=>'Email'
			],
			[
				'field'=>'no_telpon',
				'rules'=>'trim|required',
				'label'=>'No Telepon'
			],
			[
				'field'=>'alamat_instansi',
				'rules'=>'trim|required',
				'label'=>'Alamat Instansi'
			],
		];
	}
	public function validate(){
        $rules = $this->aboutRules();
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }
}