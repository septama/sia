<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun_model extends CI_Model{
    private $table = 'akun';

    public function getAkun(){
        return $this->db->get($this->table)->result();
    }

    public function getAkunByMonthYear($bulan,$tahun){
        return $this->db->select('akun.no_reff,akun.nama_reff,akun.keterangan,tipe_akun,transaksi.tgl_transaksi')
                        ->from($this->table)
                        ->where('month(transaksi.tgl_transaksi)',$bulan)
                        ->where('year(transaksi.tgl_transaksi)',$tahun)
                        ->join('transaksi','transaksi.no_reff = akun.no_reff')
                        ->group_by('akun.nama_reff')
                        ->order_by('akun.no_reff')
                        ->get()
                        ->result();
    }

    public function countAkunByNama($str){
        return $this->db->where('nama_reff',$str)->get($this->table)->num_rows();
    }

    public function countAkunByNoReff($str){
        return $this->db->where('no_reff',$str)->get($this->table)->num_rows();
    }

    public function getAkunByNo($noReff){
        return $this->db->where('no_reff',$noReff)->get($this->table)->row();
    }

    public function insertAkun($data){
        return $this->db->insert($this->table,$data);
    }

    public function updateAkun($noReff,$data){
        return $this->db->where('no_reff',$noReff)->update($this->table,$data);
    }

    public function deleteAkun($noReff){
        return $this->db->where('no_reff',$noReff)->delete($this->table);
    }

    public function getDefaultValues(){
        return [
            'no_reff'=>'',
            'nama_reff'=>'',
            'keterangan'=>'',
            'tipe_akun'=>''
        ];
    }

    public function getValidationRules(){
        return [
            [
                'field'=>'no_reff',
                'label'=>'No.Reff',
                'rules'=>'trim|required|numeric'
            ],
            [
                'field'=>'nama_reff',
                'label'=>'Nama Reff',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'keterangan',
                'label'=>'Keterangan',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'tipe_akun',
                'label'=>'Tipe Akun',
                'rules'=>'trim|required'
            ],
        ];
    }

    public function validate($data = null){
        $rules = $this->getValidationRules();
        if(isset($data)){
            if($data->old_no_reff!=$data->no_reff){
                $rules[0]['rules'] .= '|callback_isNoAkunThere'; 
            }
            if($data->old_nama_reff!=$data->nama_reff){
                $rules[1]['rules'] .= '|callback_isNamaAkunThere'; 
            }
        }
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_error_delimiters('<span class="text-danger" style="font-size:14px">','</span>');
        return $this->form_validation->run();
    }
    public function getNumReffAkun(){
        $this->db->from($this->table);
        $this->db->order_by('no_reff','desc');
        $result = $this->db->get();
        if($result->num_rows()==0){
            return "001";
        }
        else{
            $data=$result->row_array();
            $num = intval($data['no_reff']);
            $num = $num + 1;
            $no_reff='';
            $num_length = strlen((string)$num);
            for ($i=1; $i<$num_length; $i++) { 
                # code...
                $no_reff = $no_reff.'0';
            }
            return $no_reff = $no_reff.$num; 
        }
    }
}