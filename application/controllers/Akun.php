<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller{
	public function __construct(){
		parent::__construct();
        $this->load->helper(['url','form','sia','tgl_indo']);
        $this->load->library(['session','form_validation']);
        $this->load->model('Akun_model','akun',true);
        $this->load->model('Jurnal_model','jurnal',true);
        $this->load->model('Transaksi_model','transaksi',true);

        $login = $this->session->userdata('login');
        if(!$login){
            redirect('login');
        }
        else{
            if($this->session->userdata('level')!='admin'){
               redirect('error_web/privilages_user');
            }
        }
	}
	public function index(){
        $content = 'akun/data_akun';
        $titleTag = 'Data Akun';
        $dataAkun = $this->akun->getAkun();
        $this->load->view('template',compact('content','dataAkun','titleTag'));
    }

    public function isNamaAkunThere($str){
        $namaAkun = $this->akun->countAkunByNama($str);
        if($namaAkun >= 1){
            $this->form_validation->set_message('isNamaAkunThere', 'Nama Akun Sudah Ada');
            return false;
        }
        return true;
    }

    public function isNoAkunThere($str){
        $noAkun = $this->akun->countAkunByNoReff($str);
        if($noAkun >= 1){
            $this->form_validation->set_message('isNoAkunThere', 'No.Reff Sudah Ada');
            return false;
        }
        return true;
    }

    public function add(){
        $title = 'Tambah';
        $titleTag = 'Tambah Data Akun';
        $action = 'akun/add';
        $content = 'akun/form_akun';
        $noReff = $this->akun->getNumReffAkun();

        if(!$_POST){
            $data = (object) $this->akun->getDefaultValues();
            $data->no_reff = $noReff;
        }else{
            $data = (object) $this->input->post(null,true);
            $data->id_user = $this->session->userdata('id');
        }

        if(!$this->akun->validate()){
            $this->load->view('template',compact('content','title','action','data','titleTag','noReff'));
            return;
        }
        
        $this->akun->insertAkun($data);
        $this->session->set_flashdata('berhasil','Data Akun Berhasil Di Tambahkan');
        redirect('akun');
    }

    public function edit($no_reff = null){
        $title = 'Edit';
        $titleTag = 'Edit Data Akun';
        $action = 'akun/edit/'.$no_reff;
        $content = 'akun/form_akun';

        if(!$_POST){
            $data = (object) $this->akun->getAkunByNo($no_reff);
            $data->old_nama_reff = $data->nama_reff;
            $data->old_no_reff = $data->no_reff;
        }else{
            $data = (object) $this->input->post(null,true);
            $data_insert->nama_reff = $data->nama_reff;
            $data_insert->keterangan = $data->keterangan;
            $data_insert->no_reff = $data->no_reff;
            $data_insert->tipe_akun = $data->tipe_akun;
            $data_insert->id_user = $this->session->userdata('id');
            $data->id_user = $this->session->userdata('id');
        }
        if(!$this->akun->validate($data)){
            $this->load->view('template',compact('content','title','action','data','titleTag'));
            return;
        }
        $this->akun->updateAkun($no_reff,$data_insert);
        $this->session->set_flashdata('berhasil','Data Akun Berhasil Di Ubah');
        redirect('akun');
    }

    public function delete(){
        $id = $this->input->post('id',true);
        $noReffTransaksi = $this->transaksi->countTransaksiNoReff($id);
        if($noReffTransaksi >= 0 ){
            $this->session->set_flashdata('dataNull','No.Reff '.$id.' Tidak Bisa Di Hapus Karena Data Akun Ada Di Transaksi');
            redirect('akun');
        }
        $this->akun->delete($id);
        $this->session->set_flashdata('berhasilHapus','Data akun dengan No.Reff '.$id.' berhasil di hapus');
        redirect('akun');
    }
    public function getAkun(){
        $akun = $this->akun->getAkun();
        if(isset($akun)){
            echo json_encode(['success'=>true,'akun'=>(array)$akun]);
        }
        else{
            echo json_encode(['success'=>false]);
        }
    }
}