<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper(['url','form','sia','tgl_indo']);
        $this->load->library(['session','form_validation']);
        $this->load->model('Akun_model','akun',true);
        $this->load->model('Jurnal_model','jurnal',true);
        $this->load->model('User_model','user',true);
        $login = $this->session->userdata('login');
        if(!$login){
            redirect('login');
        }
    }

    public function index(){
        $titleTag = 'Dashboard';
        $content = 'user/dashboard';
        $dataAkun = $this->akun->getAkun();
        $dataAkunTransaksi = $this->jurnal->getAkunInJurnal();
        foreach($dataAkunTransaksi as $row){
            $data[] = (array) $this->jurnal->getJurnalByNoReff($row->no_reff);
            $saldo[] = (array) $this->jurnal->getJurnalByNoReffSaldo($row->no_reff);
        }

        // if($data == null || $saldo == null){
        //     $data = 0;
        //     $saldo = 0;
        // }
        if(isset($data)){
            $jumlah = count($data);   
        }

        $jurnals = $this->jurnal->getJurnalJoinAkun();
        $totalDebit = $this->jurnal->getTotalSaldo('debit');
        $totalKredit = $this->jurnal->getTotalSaldo('kredit');
        $this->load->view('template',compact('content','dataAkun','titleTag','jurnals','totalDebit','totalKredit','jumlah','data','saldo','dataAkunTransaksi'));
    }
    public function data_user(){
        if($this->session->userdata('level')=='admin'){
            $titleTag = 'User';
            $content = 'user/user';
            $dataUser = $this->user->getUser()->result();
            $this->load->view('template',compact('content','dataUser','titleTag'));
        }
        else{
            $this->edit($this->session->userdata('id'));
        }
        
    }
    public function checkUsername(){
        $user = $this->user->getUser(['username'=>$this->input->post('username')]);
        if($user->num_rows()>0){
            $this->form_validation->set_message('checkUsername', 'Username sudah digunakan oleh user lain');
            return false;
        }
        return true;
    }
    public function checkEmail(){
        $user = $this->user->getUser(['email'=>$this->input->post('email')]);
        if($user->num_rows()>0){
            $this->form_validation->set_message('checkEmail', 'Email sudah digunakan oleh user lain');
            return false;
        }
        return true;
    }
    public function add(){
        $title = 'Tambah';
        $titleTag = 'Tambah Data User';
        $action = 'user/add';
        $content = 'user/form_user';
        $add = true;
        if(!$_POST){
            $data = (object) $this->user->getDefaultValuesUser();
            $data->id_user = $data->id = '';
            $data->old_username = '';
            $data->old_email = '';
        }else{
            $data = (object) $this->input->post(null,true);
            $data_insert = [
                'email' => $data->email,
                'username'=> $data->username,
                'password'=> md5($data->password),
                'no_hp'=>$data->no_hp,
                'nama'=>$data->nama,
                'level'=>$data->level
            ]; 
        }

        if(!$this->user->validateFormUser($data)){
            $this->load->view('template',compact('content','title','action','data','titleTag','add'));
            return;
        }
        
        $this->user->insertUser($data_insert);
        $this->session->set_flashdata('berhasil','Data User Berhasil Di Tambahkan');
        redirect('user/data_user');
    }
    public function edit($id = null){
        $title = 'Edit';
        $titleTag = 'Edit Data User';
        $action = 'user/edit/'.$id;
        $content = 'user/form_user';
        $add=false;
        if(!$_POST){
            $data = (object) $this->user->getUser(['id_user'=>$id])->row();
            $data->old_username = $data->username;
            $data->old_email = $data->email;
        }else{
            $data = (object) $this->input->post(null,true);
            $data_insert['username'] = $data->username;
            $data_insert['email'] = $data->email;
            $data_insert['nama'] = $data->nama;
            $data_insert['no_hp'] = $data->no_hp;
            $data_insert['level']=$data->level;
            if(isset($data->password)&&$data->password != null){
                $data_insert['password'] = md5($data->password);
            }
        }
        if(!$this->user->validateFormUser($data)){
            $this->load->view('template',compact('content','title','action','data','titleTag','add'));
            return;
        }
        $this->user->updateUser($id,$data_insert);
        $this->session->set_flashdata('berhasil','Data User Berhasil Di Ubah');
        redirect('user/data_user');
    }
    public function delete(){
        $id = $this->input->post('id',true);
        $idusername = $this->input->post('username',true);
        $transaksi = $this->transaksi->getTransaksi(['id_user'=>$id]);
        if($transaksi->num_rows() >= 0 ){
            $this->session->set_flashdata('dataNull','User dengan username '.$username.' Tidak Bisa Di Hapus Karena Data Username Ada Di Transaksi');
            redirect('akun');
        }
        $this->akun->delete($id);
        $this->session->set_flashdata('berhasilHapus','User dengan username '.$username.' berhasil di hapus');
        redirect('akun');
    }

    public function neracaSaldo(){
        $titleTag = 'Neraca Saldo';
        $content = 'user/neraca_saldo_main';
        $listJurnal = $this->jurnal->getJurnalByYearAndMonth();
        $tahun = $this->jurnal->getJurnalByYear();
        $this->load->view('template',compact('content','listJurnal','titleTag','tahun'));
    }

    public function neracaSaldoDetail(){
        $content = 'user/neraca_saldo';
        $titleTag = 'Neraca Saldo';

        $bulan = $this->input->post('bulan',true);
        $tahun = $this->input->post('tahun',true);

        if(empty($bulan) || empty($tahun)){
            redirect('neraca_saldo');
        }

        $dataAkun = $this->akun->getAkunByMonthYear($bulan,$tahun);
        $data = null;
        $saldo = null;
        
        foreach($dataAkun as $row){
            $data[] = (array) $this->jurnal->getJurnalByNoReffMonthYear($row->no_reff,$bulan,$tahun);
            $saldo[] = (array) $this->jurnal->getJurnalByNoReffSaldoMonthYear($row->no_reff,$bulan,$tahun);
        }

        if($data == null || $saldo == null){
            $this->session->set_flashdata('dataNull','Neraca Saldo Dengan Bulan '.bulan($bulan).' Pada Tahun '.date('Y',strtotime($tahun)).' Tidak Di Temukan');
            redirect('neraca_saldo');
        }

        $jumlah = count($data);

        $this->load->view('template',compact('content','titleTag','dataAkun','data','jumlah','saldo'));
    }

    public function logout(){
        $this->user->logout();
        redirect('');
    }
}
