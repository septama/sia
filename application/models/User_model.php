<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{
    private $table = 'user';

    public function getDefaultValuesLogin(){
        return [
            'username'=>'',
            'password'=>'',
        ];
    }

    public function getValidationRulesLogin(){
        return [
            [
                'field'=>'username',
                'label'=>'Username',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'password',
                'label'=>'Password',
                'rules'=>'trim|required'
            ]
        ];
    }
    public function getDefaultValuesUser(){
        return [
            'username'=>'',
            'email'=>'',
            'no_hp'=>'',
            'password'=>'',
            'nama'=>'',
            'confirm_password'=>'',
            'level'=>'admin',
        ];
    }
    public function getValidationRulesFormUser(){
        return [
            [
                'field'=>'username',
                'label'=>'Username',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'password',
                'label'=>'Password',
                'rules'=>'trim'
            ],
            [
                'field'=>'confirm_password',
                'label'=>'Confirm Password',
                'rules'=>'trim'
            ],
            [
                'field'=>'email',
                'label'=>'Email',
                'rules'=>'trim|required'
            ]
        ];
    }

    public function validateLogin(){
        $rules = $this->getValidationRulesLogin();
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }
    public function validateFormUser($data = null){
        $rules = $this->getValidationRulesFormUser();
        if(isset($data)){
            if($data->old_username!=$data->username){
                $rules[0]['rules'] .= '|callback_checkUsername'; 
            }
            if($data->old_email!=$data->email){
                $rules[3]['rules'] .= '|callback_checkEmail'; 
            }
            if(isset($data->password) && $data->password != null){
                $rules[2]['rules'] .= '|required|matches[password]';
            }
        }
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function run($data){
        $username = $data->username;
        $password = md5($data->password);

        $user = $this->db->where('username',$username)
                             ->where('password',$password)
                             ->get($this->table);
        
        if($user->num_rows()>0){
            $sessionData = [
                'login'=>true,
                'username'=>$user->row()->nama,
                'id'=>$user->row()->id_user,
                'level'=>$user->row()->level
            ];
            $this->session->set_userdata($sessionData);
            return true;
        }

        return false;
    }

    public function logout(){
        $sessionData = ['login','username','id'];
        $this->session->unset_userdata($sessionData);
        $this->session->sess_destroy();
    }
    public function insertUser($data){
        return $this->db->insert($this->table, $data);
    }
    public function getUser($where = null){
        if($where){
            return $this->db->where($where)->get('user');
        }
        return $this->db->get('user');
    }
    public function updateUser($id,$data){
        return $this->db->where('id_user',$id)->update($this->table,$data);
    }
    public function delete($id){
        return $this->db->where('id_user',$id)->delete($this->table);
    }
}
?>