var base_url = window.location.origin+'/ams/';
$("#btn_add_row").on("click",function(){
	var url = base_url+'akun/getAkun/';
	var table_length = $("#table-transaksi tbody tr").length;
	var row_id = table_length+1;
	//var dropdownAkun = '<select onchange="getNamaReff(this,'+row_id+');" class="form-control" name="no_reff[]" id="no_reff-'+row_id+'"><option value="null">Pilih Akun</option>';
	/*$.ajax({
		url:url,
		type:'get',
		dataType:'json',
		async:false,
		success:function(response){
			if(response.success){
				for(i=0; i<response.akun.length; i++){
					dropdownAkun +='<option value="'+response.akun[i].no_reff+'">'+response.akun[i].nama_reff+'</option>';
				}
			}
			else{
				swal({
					title: 'Oops, something wrong with the server',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'ok'
				}).then((result) => {
					if (result) {
						location.reload;
					}
				});
			}
		}
	});
	dropdownAkun +="</select>";*/
	var html = '<tr id="row-'+row_id+'"><td>'+
                        '<div class="col">'+
                          '<input type="text" class="form-control" name="deskripsi[]" id="deskripsi-"'+row_id+'>'+
                      '</div>'+
                      '</td>'+
                      '<td>'+
                        '<div class="col">'+
                          '<select class="form-control" id="jenis_saldo" name="jenis_saldo[]">'+
                            '<option value="debit">Debit</option>'+
                            '<option value="kredit">Kredit</option>'+
                          '</select>'+
                      '</div></td>'+
                      '<td>'+
                        '<div class="col">'+
                          '<input type="number" name="saldo[]" class="form-control saldo" id="saldo" >'+
                      '</div></td>'+
                      '<td>'+
                        '<div class="col">'+
                          '<button type="button" class="form-control btn btn-danger" onclick="removeRow('+row_id+')">Delete</button>'+
                      '</div></td></tr>';
    if(table_length >= 1) {
        $("#table-transaksi tbody tr:last").after(html);  
    }
    else {
       $("#table-transaksi tbody").html(html);
    }
});
function getNamaReff(req){
	if(req.value=='null'||req.value==undefined||req.value==null){
	} else{
		$('#no_reff').val(req.value);
	}
}
function deleteTransaksi(kode_transaksi){
	if(kode_transaksi!=null){
		swal({
			title: 'Apakah kamu yakin akan menghapus transaksi dengan kode '+kode_transaksi+' ini ?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'ok'
				}).then((result) => {
					if (result) {
						$.ajax({
							url:base_url+'transaksi/delete',
							type:'post',
							data:{kode:kode_transaksi},
							dataType:'json',
							async:false,
							success:function(response){
								if(response.success){
									swal({
										title:'Sukses menghapus transaksi dengan kode'+kode_transaksi,
										type:'success',
										confirmButtonColor: '#3085d6',
										confirmButtonText:'ok',
									}).then((result)=>{
										if(result){
											window.location.href=base_url+'transaksi';
										}
									});
								}
								else{
									swal({
										title:'Gagal menghapus transaksi dengan kode'+kode_transaksi,
										type:'warning',
										confirmButtonColor: '#3085d6',
										confirmButtonText:'ok',
									}).then((result)=>{
										if(result){
											window.location.href=base_url+'transaksi';
										}
									});
								}
							}
						})
				}
		});
	}
}
function removeRow(row){
	if(row){
		$("#table-transaksi tbody tr#row-"+row).remove();
	}
}
function deleteTransaksiItem(param){
	var id =  $(param).data('id');
	if(id){
		swal({
			title: 'Apakah kamu yakin akan menghapus transaksi item ini ?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'ok'
		}).then((result)=>{
			if(result){
				$.ajax({
					async:false,
					url:base_url+'transaksi/deleteTransaksiItem',
					type:'post',
					data:{id:id},
					dataType:'json',
					success:function(response){
						if(response.success){
							swal({
								title:'Sukses menghapus transaksi item',
								type:'success',
								confirmButtonColor: '#3085d6',
								confirmButtonText:'ok',
								}).then((result)=>{
									if(result){
										window.location.href=base_url+'transaksi';
									}
								});
						}else{
								swal({
									title:'Gagal menghapus transaksi item',
									type:'warning',
									confirmButtonColor: '#3085d6',
									confirmButtonText:'ok',
								}).then((result)=>{
									if(result){
										window.location.href=base_url+'transaksi';
									}
								});
							}
					}
				})
			}
		});
	}
}