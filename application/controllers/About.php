<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
        $this->load->helper(['url','form','sia','tgl_indo']);
        $this->load->library(['session','form_validation']);
		$this->load->model('About_model','about',true);
		$login = $this->session->userdata('login');
        if(!$login){
            redirect('login');
        }
	}
	public function index(){
		$titleTag = 'About';
		$title="Edit";
        $content = 'about/about';
        $url_back = base_url('/dashboard');
        $about = $this->about->getAbout()->row();
        $url_edit = base_url('/about/edit');
        $this->load->view('template',compact('content','about','titleTag','title','url_back','url_edit'));
	}
	public function edit(){
		$titleTag = 'About';
        $content = 'about/form_about';
        $url_back = base_url('/about');
		$title="Edit";
		$action = 'about/edit';
		if(!$_POST){
        	$about = $this->about->getAbout()->row();
        	$this->load->view('template',compact('content','about','titleTag','url_back','title','action'));
		}
		else{
			$data = (object)[
				'nama_instansi'=>$this->input->post('nama_instansi',true),
				'email'=>$this->input->post('email',true),
				'foto'=>$this->input->post('foto',true),
				'no_telpon'=>$this->input->post('no_telpon',true),
				'alamat_instansi'=>$this->input->post('alamat_instansi',true)
			];
			if($this->about->validate($data)){
				$this->about->updateAbout(['id'=>$this->input->post('id',true)],$data);
				$this->session->set_flashdata('berhasil','Data About Berhasil Di ubah');
        		redirect('about');
			}
			else{
				$about = $this->about->getAbout()->row();
				$this->load->view('template',compact('content','about','titleTag','url_back','title','action'));
            	return;
			}
		}
	}
}