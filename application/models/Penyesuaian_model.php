<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Penyesuaian_model extends CI_Model
{

	public function getSaldo($where = null){
		if(isset($where)){
			$this->db->where($where);
			return $this->db->get('saldo');
		}
		return $this->db->get('saldo');
	}
	public function getAllSaldo($where=null){
		if(isset($where)){
			return $this->db->select_sum('jumlah_saldo')->where($where)->get('saldo');
		}
		return $this->db->select_sum('jumlah_saldo')->get('saldo');
	}
	public function getAllLimitSaldo($where=null){
		if(isset($where)){
			return $this->db->select_sum('jumlah_saldo')->where($where)->get('limit_saldo');
		}
		return $this->db->select_sum('jumlah_saldo')->get('limit_saldo');
	}
	public function getLimitSaldo($where = null){
		if(isset($where)){
			$this->db->where($where);
			return $this->db->get('limit_saldo');
		}
		return $this->db->get('limit_saldo');
	}
	public function insertSaldo($data = null){
		if(isset($data)){
			return $this->db->insert('saldo', $data);
		}
		return false;
	}
	public function insertLimitSaldo($data = null){
		if(isset($data)){
			return $this->db->insert('limit_saldo', $data);
		}
		return false;
	}
	public function updateSaldo($where = null,$data){
		if(isset($where)){
			$this->db->where($where);
			return $this->db->update('saldo',$data);
		}
		return false;
	}
	public function updateLimitSaldo($where = null,$data){
		if(isset($where)){
			$this->db->where($where);
			return $this->db->update('limit_saldo',$data);
		}
		return false;
	}
	public function deleteSaldo($where){
		if(isset($where)){
			$this->db->where($where);
			return $this->db->delete('saldo');
		}
		return false;
	}
	public function deleteLimitSaldo($where){
		if(isset($where)){
			$this->db->where($where);
			return $this->db->delete('limit_saldo');
		}
		return false;
	}
	public function getDefaultValues(){
		return [
			'tgl_input'=>'',
			'deskripsi'=>'',
			'jumlah_saldo'=>''
		];
	}
	public function getValidationRules(){
		return [
			[
				'field'=>'jumlah_saldo',
				'rules'=>'trim|required|numeric',
				'label'=>'Jumlah Saldo'
			],
			[
				'field'=>'deskripsi',
				'rules'=>'trim|required'
			],
		];
	}
	public function validate(){
        $rules = $this->getValidationRules();
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_error_delimiters('<span class="text-danger" style="font-size:14px">','</span>');
        return $this->form_validation->run();
    }
}