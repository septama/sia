  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Data User</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            
          </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="<?= base_url('assets/img/theme/team-4-800x800.jpg') ?>">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold"><?= ucwords($this->session->userdata('username')) ?></span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <a href="<?= base_url('logout') ?>" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">    
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col-xl-8 mb-5 mb-xl-0">
               
        </div>
      </div>
      <div class="row mt-5">
        <div class="col mb-5 mb-xl-0">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-3"><?= $title ?> Data User</h3>
                </div>
                <div class="col-12 my-3">
                  <form action="<?= base_url($action) ?>" method="post">
                    <div class="form-group" style="display: none;">
                        <label for="no_ref">ID</label>
                        <p><?= form_error('id_user') ?></p>
                        <input type="text" name="id_user" class="form-control mb-3" readonly id="id" value="<?= $data->id_user ?>">
                      </div>
                      <div class="form-group" style="display: none;">
                        <label for="no_ref">Username</label>
                        <input type="text" name="old_username" class="form-control mb-3" readonly id="old_username" value="<?= $data->username ?>">
                      </div>
                      <div class="form-group">
                        <label for="username">Username</label>
                        <p><?= form_error('username') ?></p>
                        <input type="text" name="username" class="form-control mb-3" id="username" value="<?= $data->username ?>">
                      </div>
                      <div class="form-group">
                        <label for="nama">Nama</label>
                        <p><?= form_error('nama') ?></p>
                        <input type="text" name="nama" class="form-control mb-3" id="nama" value="<?= $data->nama ?>">
                      </div>
                      <div class="form-group" style="display: none;">
                        <label for="old_email">Email</label>
                        <input type="email" name="old_email" class="form-control mb-3" id="old_email" value="<?= $data->email ?>">
                      </div>
                      <?php

                        
                        if($this->session->userdata('level')=='admin'){
                          $html = '<div class="form-group"><label>Level</label>';
                        }
                        else{
                          $html = '<div class="form-group" style="display:none"><label>Level</label>';
                        }
                        $html .= '<select class="form-control" id="level" name="level">';
                        $level = ['admin','keuangan','direktur'];
                        for($i=0; $i<count($level); $i++){
                          if($level[$i]==$data->level){
                            $html .= '<option value="'.$level[$i].'" selected>'.$level[$i].'</option>';
                          }
                          else{
                            $html .= '<option value="'.$level[$i].'">'.$level[$i].'</option>';
                          }
                        }
                        $html .='</select></div>';
                        echo $html;
                      ?>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <p><?= form_error('email') ?></p>
                        <input type="email" name="email" class="form-control mb-3" id="email" value="<?= $data->email ?>">
                      </div>
                      <div class="form-group">
                        <label for="no_hp">No HP</label>
                        <p><?= form_error('no_hp') ?></p>
                        <input name="no_hp" id="no_hp" class="form-control mb-3" value="<?= $data->no_hp ?>">
                      </div>
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control mb-3">
                      </div>
                      <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <p><?= form_error($data->password); ?></p>
                        <input type="password" id="confirm_password" name="confirm_password" class="form-control mb-3">
                      </div>
                      <div class="form-group">
                        <?php echo ($add==false) ? '<p>Jika tidak ingin mengubah password, jangan di isi field password</p>' : ''; ?> 
                      </div>
                      <div class="col-12 mt-4">
                        <button type="submit" class="btn-primary btn" id="button_akun"><?= $title ?></button>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>