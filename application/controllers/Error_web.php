<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_web extends CI_Controller{

	public function privilages_user(){
		$this->load->view('errors/html/error_404',['heading'=>'Access Privilages','message'=>'You dont have permissions to access this page']);
	}
}