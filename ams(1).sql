-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 22, 2019 at 11:21 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `nama_instansi` varchar(255) NOT NULL,
  `alamat_instansi` text NOT NULL,
  `no_telpon` varchar(16) NOT NULL,
  `email` varchar(24) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `nama_instansi`, `alamat_instansi`, `no_telpon`, `email`, `foto`) VALUES
(1, 'PT. Ale Interior', 'Perum Rajeg Asri', '08811100001', 'ale2@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `no_reff` varchar(8) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tipe_akun` enum('1','2') NOT NULL DEFAULT '1',
  `nama_reff` varchar(40) NOT NULL,
  `keterangan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`no_reff`, `id_user`, `tipe_akun`, `nama_reff`, `keterangan`) VALUES
('001', 2, '1', 'Kas', 'Kas'),
('002', 1, '1', 'Piutang', 'Piutang Usaha'),
('003', 1, '1', 'Perlengkapan', 'Perlengkapan Perusahaan'),
('004', 1, '1', 'Peralatan', 'Peralatan Perusahaan'),
('005', 1, '1', 'Akumulasi Peralatan', 'Akumulasi Peralatan'),
('006', 1, '1', 'Utang Usaha', 'Utang Usaha'),
('007', 1, '1', 'Modal', 'Modal'),
('008', 1, '1', 'Prive', 'Prive'),
('009', 1, '1', 'Pendapatan', 'Pendapatan'),
('010', 2, '1', 'Beban Gaji', 'Beban Gaji'),
('011', 1, '1', 'Beban Sewa', 'Beban Sewa'),
('012', 1, '1', 'Beban Penyusutan Peralatan', 'Beban Penyusutan Peralatan'),
('013', 1, '1', 'Beban Lat', 'Beban Lat'),
('014', 1, '1', 'Beban Perlengkapan', 'Beban Perlengkapan');

-- --------------------------------------------------------

--
-- Table structure for table `limit_saldo`
--

CREATE TABLE `limit_saldo` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jumlah_saldo` int(11) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `tgl_input` date NOT NULL DEFAULT current_timestamp(),
  `waktu_input` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `limit_saldo`
--

INSERT INTO `limit_saldo` (`id`, `id_user`, `jumlah_saldo`, `deskripsi`, `tgl_input`, `waktu_input`) VALUES
(1, 1, 300000, 'aasaaa', '2019-07-18', '2019-07-16 07:24:17');

-- --------------------------------------------------------

--
-- Table structure for table `saldo`
--

CREATE TABLE `saldo` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jumlah_saldo` int(11) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `tgl_input` date NOT NULL DEFAULT current_timestamp(),
  `waktu_input` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saldo`
--

INSERT INTO `saldo` (`id`, `id_user`, `jumlah_saldo`, `deskripsi`, `tgl_input`, `waktu_input`) VALUES
(1, 1, 200000, 'Penambahan Saldoo', '2019-07-10', '2019-07-19 14:17:12'),
(4, 1, 111000, 'a', '2019-07-26', '2019-07-20 13:57:00');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `kode` varchar(12) NOT NULL,
  `no_reff` varchar(8) NOT NULL,
  `memo` varchar(255) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_transaksi` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `kode`, `no_reff`, `memo`, `tgl_input`, `tgl_transaksi`) VALUES
(63, 1, 'VKK001', '003', 'asd', '2019-07-19 08:28:18', '2019-07-15');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_item`
--

CREATE TABLE `transaksi_item` (
  `id` int(11) NOT NULL,
  `kode` varchar(12) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `saldo` int(11) NOT NULL,
  `jenis_saldo` enum('debit','kredit') NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `waktu_input` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_item`
--

INSERT INTO `transaksi_item` (`id`, `kode`, `deskripsi`, `saldo`, `jenis_saldo`, `tgl_transaksi`, `waktu_input`) VALUES
(7, 'VKK001', 'Meja Kantor', 5000, 'kredit', '2019-07-15', '2019-07-19 08:28:18'),
(31, 'VKK001', 'Komputer', 2000, 'kredit', '2019-07-15', '2019-07-19 08:28:18'),
(32, 'VKK001', 'Kulkas', 4000, 'kredit', '2019-07-15', '2019-07-19 08:28:18');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `level` enum('keuangan','direktur','admin') NOT NULL DEFAULT 'admin',
  `no_hp` varchar(16) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `level`, `no_hp`, `username`, `password`, `last_login`) VALUES
(1, 'Hidayat Chandra', 'hidayatchandra08@gmail.com', 'admin', '082278301833', 'hidayat', 'ff0ad81f45767173a8503662f2d13c2c', '2019-07-20 23:47:09'),
(2, 'nanda nur septama', 'nandanurseptama@gmail.com', 'direktur', '089647045539', 'nandans', 'ff0ad81f45767173a8503662f2d13c2c', '2019-07-20 11:15:59'),
(3, 'Leroy Sane', 'leroysane@gmail.com', 'direktur', '8900001', 'leroysane', 'ff0ad81f45767173a8503662f2d13c2c', '0000-00-00 00:00:00'),
(4, 'Loris Karius', 'loriskariuss@gmail.com', 'direktur', '082278301833', 'loriskarius', 'ff0ad81f45767173a8503662f2d13c2c', '2019-07-20 08:40:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`no_reff`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `limit_saldo`
--
ALTER TABLE `limit_saldo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saldo`
--
ALTER TABLE `saldo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD UNIQUE KEY `kode` (`kode`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `transaksi_item`
--
ALTER TABLE `transaksi_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `limit_saldo`
--
ALTER TABLE `limit_saldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `saldo`
--
ALTER TABLE `saldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `transaksi_item`
--
ALTER TABLE `transaksi_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `akun`
--
ALTER TABLE `akun`
  ADD CONSTRAINT `akun_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
