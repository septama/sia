<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal_umum extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->helper(['url','form','sia','tgl_indo']);
        $this->load->library(['session','form_validation']);
        $this->load->model('Jurnal_model','jurnal',true);
        $this->load->model('Penyesuaian_model','penyesuaian',true);
        $login = $this->session->userdata('login');
        if(!$login){
            redirect('login');
        }
        else{
            if($this->session->userdata('level')!='admin'){
               redirect('error_web/privilages_user');
            }
        }
    }
	public function index(){
		$titleTag = 'Jurnal Umum';
        $content = 'jurnal/jurnal_umum_main';
        $listJurnal = $this->jurnal->getJurnalByYearAndMonth();
        $tahun = $this->jurnal->getJurnalByYear();
        $this->load->view('template',compact('content','listJurnal','titleTag','tahun'));
	}
	public function jurnalUmumDetail(){
        $content = 'jurnal/jurnal_umum';
        $titleTag = 'Jurnal Umum';

        $bulan = $this->input->post('bulan',true);
        $tahun = $this->input->post('tahun',true);
        $jurnals = null;

        if(empty($bulan) || empty($tahun)){
            redirect('jurnal_umum');
        }

        $jurnals = $this->jurnal->getJurnalJoinAkunDetail($bulan,$tahun);
        $totalDebit = $this->jurnal->getTotalSaldoDetail('debit',$bulan,$tahun);
        $totalKredit = $this->jurnal->getTotalSaldoDetail('kredit',$bulan,$tahun);
        $saldo = $this->penyesuaian->getSaldo(['month(tgl_input)'=>$bulan,'year(tgl_input)'=>$tahun])->result();
        if($jurnals==null){
            $this->session->set_flashdata('dataNull','Data Jurnal Dengan Bulan '.bulan($bulan).' Pada Tahun '.date('Y',strtotime($tahun)).' Tidak Di Temukan');
            redirect('jurnal_umum');
        }
        $this->load->view('template',compact('content','jurnals','totalDebit','totalKredit','titleTag','saldo'));
    }
}