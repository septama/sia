<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyesuaian extends CI_Controller{
	
	public function __construct(){
        parent::__construct();
        $this->load->helper(['url','form','sia','tgl_indo']);
        $this->load->library(['session','form_validation']);
        $this->load->model('Penyesuaian_model','penyesuaian',true);
        $this->load->model('Transaksi_model','transaksi',true);
        $login = $this->session->userdata('login');
        if(!$login){
            redirect('login');
        }
        else{
            if($this->session->userdata('level')!='admin'){
               redirect('error_web/privilages_user');
            }
        }
    }
    public function index(){
    	$titleTag = 'Penyesuaian';
        $content = 'penyesuaian/penyesuaian';
        $url_back = base_url('dashboard');
        $url_saldo = base_url('penyesuaian/data_saldo');
        $url_limit_saldo = base_url('penyesuaian/data_limit_saldo');
        $this->load->view('template',compact('content','titleTag','url_back','url_limit_saldo','url_saldo'));
    }
	public function data_saldo(){
		$titleTag = 'Penyesuaian';
        $content = 'penyesuaian/saldo';
        $url_back = base_url('penyesuaian');
        $listSaldo = $this->penyesuaian->getSaldo()->result();
        $this->load->view('template',compact('content','listSaldo','titleTag','url_back'));
	}
	public function data_limit_saldo(){
		$titleTag = 'Penyesuaian';
        $content = 'penyesuaian/limit';
        $url_back = base_url('penyesuaian');
        $listLimitSaldo = $this->penyesuaian->getLimitSaldo()->result();
        $this->load->view('template',compact('content','listLimitSaldo','titleTag','url_back'));
	}
	public function add_saldo(){
        $title = 'Tambah'; 
        $content = 'penyesuaian/form_saldo'; 
        $action = 'penyesuaian/add_saldo'; 
        $tgl_input = date('Y-m-d H:i:s'); 
        $id_user = $this->session->userdata('id');
        $titleTag = 'Tambah Saldo';
        $url_back = base_url('penyesuaian/data_saldo');

        $start_date = date("y-m-1");
        $end_date = date("y-m-t");
        $saldoKredit = $this->transaksi->getTotalSaldoBy(['jenis_saldo'=>'kredit','transaksi_item.tgl_transaksi >='=>$start_date,'transaksi_item.tgl_transaksi <='=>$end_date]);
        $saldoDebit = $this->transaksi->getTotalSaldoBy(['jenis_saldo'=>'debit','transaksi_item.tgl_transaksi >='=>$start_date,'transaksi_item.tgl_transaksi <='=>$end_date]);
        $limit = $this->penyesuaian->getAllLimitSaldo(['limit_saldo.tgl_input >='=>$start_date,'limit_saldo.tgl_input <='=>$end_date])->row();
        $jumlahSaldo = $this->penyesuaian->getAllSaldo(['saldo.tgl_input >='=>$start_date,'saldo.tgl_input <='=>$end_date])->row();
        $sisa_saldo = 0;
        $sisa_saldo += isset($saldoDebit->saldo) ? $saldoDebit->saldo : 0;
        $sisa_saldo -= isset($saldoKredit->saldo) ? $saldoKredit->saldo : 0;
        $sisa_saldo += isset($jumlahSaldo->jumlah_saldo) ? $jumlahSaldo->jumlah_saldo : 0;
        if(!$_POST){
            $data = (object) $this->penyesuaian->getDefaultValues();
            $data->old_jumlah_saldo = $data->jumlah_saldo;
        }else{
            $saldo = (object)[
                'id_user'=>$id_user,
                'deskripsi'=>$this->input->post('deskripsi',true),
                'jumlah_saldo'=>$this->input->post('jumlah_saldo',true),
                'waktu_input'=>$tgl_input,
                'tgl_input'=>$this->input->post('tgl_input',true),
            ];
            $sisa_saldo = $this->input->post('sisa_saldo');
            $limit = $this->input->post('limit');
            if(((int)$sisa_saldo+(int)$saldo->jumlah_saldo)>(int)$limit){
                $this->session->set_flashdata('dataNull','Data Saldo gagal di tambahkan karena akan melebihi limit, coba kurangi jumlah saldo yang diinputkan');
                redirect('penyesuaian/add_saldo');
            }
        }

        if(!$this->penyesuaian->validate()){
        	$data = (object) $this->penyesuaian->getDefaultValues();
            $this->load->view('template',compact('content','title','action','url_back','data','titleTag','sisa_saldo','limit'));
            return;
        }
        
        $this->penyesuaian->insertSaldo($saldo);
        $this->session->set_flashdata('berhasil','Data Saldo Berhasil Di Tambahkan');
        redirect('penyesuaian/data_saldo');    
    }
    public function add_limit_saldo(){
        $title = 'Tambah'; 
        $content = 'penyesuaian/form_limit'; 
        $action = 'penyesuaian/add_limit_saldo'; 
        $tgl_input = date('Y-m-d H:i:s'); 
        $id_user = $this->session->userdata('id');
        $titleTag = 'Tambah Saldo';
        $url_back = base_url('penyesuaian/data_limit_saldo');
        if(!$_POST){
            $data = (object) $this->penyesuaian->getDefaultValues();
        }else{
            $saldo = (object)[
                'id_user'=>$id_user,
                'deskripsi'=>$this->input->post('deskripsi',true),
                'jumlah_saldo'=>$this->input->post('jumlah_saldo',true),
                'waktu_input'=>$tgl_input,
                'tgl_input'=>$this->input->post('tgl_input',true),
            ];
        }

        if(!$this->penyesuaian->validate()){
        	$data = (object) $this->penyesuaian->getDefaultValues();
            $this->load->view('template',compact('content','url_back','title','action','data','titleTag'));
            return;
        }
        
        $this->penyesuaian->insertLimitSaldo($saldo);
        $this->session->set_flashdata('berhasil','Data Limit Saldo Berhasil Di Tambahkan');
        redirect('penyesuaian/data_limit_saldo');    
    }
    public function edit_form_saldo($id){
        if($id){
            $title = 'Edit'; 
            $content = 'penyesuaian/form_saldo'; 
            $action = 'penyesuaian/edit_saldo'; 
            $titleTag = 'Edit Saldo';
            $url_back = base_url('penyesuaian/data_saldo');
            $result = $this->penyesuaian->getSaldo(['id'=>$id]);
            $start_date = date("y-m-1");
            $end_date = date("y-m-t");
            $saldoKredit = $this->transaksi->getTotalSaldoBy(['jenis_saldo'=>'kredit','transaksi_item.tgl_transaksi >='=>$start_date,'transaksi_item.tgl_transaksi <='=>$end_date]);
            $saldoDebit = $this->transaksi->getTotalSaldoBy(['jenis_saldo'=>'debit','transaksi_item.tgl_transaksi >='=>$start_date,'transaksi_item.tgl_transaksi <='=>$end_date]);
            $limit = $this->penyesuaian->getAllLimitSaldo(['limit_saldo.tgl_input >='=>$start_date,'limit_saldo.tgl_input <='=>$end_date])->row();
            $jumlahSaldo = $this->penyesuaian->getAllSaldo(['saldo.tgl_input >='=>$start_date,'saldo.tgl_input <='=>$end_date])->row();
            $sisa_saldo = 0;
            $sisa_saldo += isset($saldoDebit->saldo) ? $saldoDebit->saldo : 0;
            $sisa_saldo -= isset($saldoKredit->saldo) ? $saldoKredit->saldo : 0;
            $sisa_saldo += isset($jumlahSaldo->jumlah_saldo) ? $jumlahSaldo->jumlah_saldo : 0;
            if($result->num_rows()>0){
            	$data = $result->row();
            }
            else{
            	$data = $this->penyesuaian->getDefaultValues();
            }
            $this->load->view('template',compact('content','url_back','title','action','data','id','titleTag','sisa_saldo','limit'));
        }else{
            redirect('penyesuaian/data_saldo');
        }
    }
    public function edit_form_limit_saldo($id){
        if($id){
            $title = 'Edit'; 
            $content = 'penyesuaian/form_limit'; 
            $action = 'penyesuaian/edit_limit_saldo'; 
            $titleTag = 'Edit Saldo';
            $url_back = base_url('penyesuaian/data_limit_saldo');
            $result = $this->penyesuaian->getLimitSaldo(['id'=>$id]);
            if($result->num_rows()>0){
            	$data = $result->row();
            }
            else{
            	$data = $this->penyesuaian->getDefaultValues();
            }
            $this->load->view('template',compact('content','url_back','title','action','data','id','titleTag'));
        }else{
            redirect('penyesuaian/data_limit_saldo');
        }
    }
    public function edit_saldo(){
    	$title = 'Edit'; 
        $content = 'penyesuaian/form_saldo'; 
        $action = 'penyesuaian/edit_saldo'; 
        $tgl_input = date('Y-m-d H:i:s'); 
        $id_user = $this->session->userdata('id');
        $titleTag = 'Edit Saldo';
        $url_back = base_url('penyesuaian/data_saldo');
        $start_date = date("y-m-1");
        $end_date = date("y-m-t");
        $saldoKredit = $this->transaksi->getTotalSaldoBy(['jenis_saldo'=>'kredit','transaksi_item.tgl_transaksi >='=>$start_date,'transaksi_item.tgl_transaksi <='=>$end_date]);
        $saldoDebit = $this->transaksi->getTotalSaldoBy(['jenis_saldo'=>'debit','transaksi_item.tgl_transaksi >='=>$start_date,'transaksi_item.tgl_transaksi <='=>$end_date]);
        $limit = $this->penyesuaian->getAllLimitSaldo(['limit_saldo.tgl_input >='=>$start_date,'limit_saldo.tgl_input <='=>$end_date])->row();
        $jumlahSaldo = $this->penyesuaian->getAllSaldo(['saldo.tgl_input >='=>$start_date,'saldo.tgl_input <='=>$end_date])->row();
        $sisa_saldo = 0;
        $sisa_saldo += isset($saldoDebit->saldo) ? $saldoDebit->saldo : 0;
        $sisa_saldo -= isset($saldoKredit->saldo) ? $saldoKredit->saldo : 0;
        $sisa_saldo += isset($jumlahSaldo->jumlah_saldo) ? $jumlahSaldo->jumlah_saldo : 0;
        if(!$_POST){
            $data = (object) $this->penyesuaian->getDefaultValues();
            $data->old_jumlah_saldo = $data->jumlah_saldo;
        }else{
        	$id = $this->input->post('id',true);
            $saldo = (object)[
                'id_user'=>$id_user,
                'deskripsi'=>$this->input->post('deskripsi',true),
                'jumlah_saldo'=>$this->input->post('jumlah_saldo',true),
                'waktu_input'=>$tgl_input,
                'tgl_input'=>$this->input->post('tgl_input',true),
            ];
            $sisa_saldo = $this->input->post('sisa_saldo');
            $limit = $this->input->post('limit');
            if(((int)$sisa_saldo+(int)$saldo->jumlah_saldo-(int)$this->input->post('old_jumlah_saldo',true))>(int)$limit){
                $this->session->set_flashdata('dataNull','Data Saldo gagal di tambahkan karena akan melebihi limit, coba kurangi jumlah saldo yang diinputkan');
                redirect('penyesuaian/edit_form_saldo/'.$id);
            }
        }

        if(!$this->penyesuaian->validate()){
        	$data = (object) $this->penyesuaian->getDefaultValues();
            $this->load->view('template',compact('content','title','url_back','action','data','titleTag','sisa_saldo','limit'));
            return;
        }
        
        $this->penyesuaian->updateSaldo(['id'=>$id],$saldo);
        $this->session->set_flashdata('berhasil','Data Saldo Berhasil Di Ubah');
        redirect('penyesuaian/data_saldo');    
    }
    public function edit_limit_saldo(){
        $title = 'Edit'; 
        $content = 'penyesuaian/form_limit'; 
        $action = 'penyesuaian/edit_limit_saldo'; 
        $tgl_input = date('Y-m-d H:i:s'); 
        $id_user = $this->session->userdata('id');
        $titleTag = 'Edit Limit Saldo';
        $url_back = base_url('penyesuaian/data_limit_saldo');
        if(!$_POST){
            $data = (object) $this->penyesuaian->getDefaultValues();
        }else{
            $id = $this->input->post('id',true);
            $saldo = (object)[
                'id_user'=>$id_user,
                'deskripsi'=>$this->input->post('deskripsi',true),
                'jumlah_saldo'=>$this->input->post('jumlah_saldo',true),
                'waktu_input'=>$tgl_input,
                'tgl_input'=>$this->input->post('tgl_input',true),
            ];
        }

        if(!$this->penyesuaian->validate()){
            $data = (object) $this->penyesuaian->getDefaultValues();
            $this->load->view('template',compact('content','url_back','title','action','data','titleTag'));
            return;
        }
        
        $this->penyesuaian->updateLimitSaldo(['id'=>$id],$saldo);
        $this->session->set_flashdata('berhasil','Data Limit Saldo Berhasil Di Ubah');
        redirect('penyesuaian/data_limit_saldo');    
    }
    public function delete_saldo(){
        $id = $this->input->post('id',true);
        if(isset($id)){
            $deleteSaldo = $this->penyesuaian->deleteSaldo(['id'=>$id]);
            if($deleteSaldo){
                $this->session->set_flashdata('berhasil','Data Saldo Berhasil Di Hapus');
                redirect('penyesuaian/data_saldo');
            }   
            else{
                $this->session->set_flashdata('berhasil','Data Saldo Gagal Di Hapus');
                redirect('penyesuaian/data_saldo');
            } 
        }
        else{
            echo json_encode(['success'=>false]);
        }
    }
    public function delete_limit_saldo(){
        $id = $this->input->post('id',true);
        if(isset($id)){
            $deleteLimitSaldo = $this->penyesuaian->deleteLimitSaldo(['id'=>$id]);
            if($deleteLimitSaldo){
                $this->session->set_flashdata('berhasil','Data Limit Saldo Berhasil Di Hapus');
                redirect('penyesuaian/data_limit_saldo');
            }   
            else{
                $this->session->set_flashdata('berhasil','Data Limit Saldo Gagal Di Hapus');
                redirect('penyesuaian/data_limit_saldo');
            } 
        }
        else{
            echo json_encode(['success'=>false]);
        }
    }   	  
}