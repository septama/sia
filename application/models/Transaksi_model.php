<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model{
    // Only admin
    private $table = 'transaksi';

    public function getTransaksi($where = null){
    	if(isset($where)){
    		$this->db->where($where);
    		return $this->db->get($this->table);
    	}else{
    		return $this->db->get($this->table);
    	}
    }
    public function viewTransaksi($where=null){
        $this->db->where($where);
        $transaksi_item = $this->db->get('transaksi_item');
        $this->db->where($where);
        $transaksi = $this->db->get('transaksi');
        return ['transaksi'=>$transaksi->row(),'transaksi_item'=>$transaksi_item];
    }
    public function getTransaksiByYearAndMonth(){
    	return $this->db->select('kode,tgl_transaksi')
                        ->from($this->table)
                        ->group_by('kode')
                        ->get();
    }
    public function getTransaksiByYear(){
        return $this->db->select('tgl_transaksi')
                        ->from($this->table)
                        ->group_by('year(tgl_transaksi)')
                        ->get();
    }	
    public function addTransaksi($transaksi,$transaksi_item){
        $res_transaksi = $this->insertTransaksi($transaksi);
        $res_transaksi_item = $this->insertTransaksiItem($transaksi_item);
        if($res_transaksi&&$res_transaksi_item){
            return true;
        }
        return false;
    }
    public function insertTransaksi($data){
        return $this->db->insert($this->table,$data);
    }
    public function insertTransaksiItem($data){
        return $this->db->insert_batch('transaksi_item', $data);
    }
    public function editTransaksi($where=null,$transaksi,$transaksi_item){
        $res_transaksi = $this->updateTransaksi($where,$transaksi);
        $res_transaksi_item = $this->updateTransaksiItem($transaksi_item);
        if($res_transaksi&&$res_transaksi_item){
            return true;
        }
        return false;
    }
    public function updateTransaksi($where = null,$data){
    	$this->db->where($where);
    	return $this->db->update($this->table,$data);
    }
    public function updateTransaksiItem($data){
        $update=true;
        for($i=0; $i<count($data);$i++){
            if(isset($data[$i]['id'])){
                $this->db->where(['id'=>$data[$i]['id']]);
                $result = $this->db->update('transaksi_item',[
                    'saldo'=>$data[$i]['saldo'],
                    'kode'=>$data[$i]['kode'],
                    'jenis_saldo'=>$data[$i]['jenis_saldo'],
                    'tgl_transaksi'=>$data[$i]['tgl_transaksi'],
                    'waktu_input'=>$data[$i]['waktu_input']]
                );
                if(!$result){
                    $update=false;
                }
            }
            else{
                $result = $this->db->insert('transaksi_item', $data[$i]);
                if(!$result){
                    $update=false;
                }
            }
        }
        if($update){
            return true;
        }
        return false;
    }
    public function removeTransaksi($where){
        $res_transaksi = $this->deleteTransaksi($where);
        $res_transaksi_item = $this->deleteTransaksiItem($where);
        if($res_transaksi&&$res_transaksi_item){
            return true;
        }
    }
    public function deleteTransaksi($where=null){
        $this->db->where($where);
        return $this->db->delete($this->table);
    }
    public function deleteTransaksiItem($where=null){
        $this->db->where($where);
        return $this->db->delete('transaksi_item');
    }
    public function getTransaksiJoinAkunDetail($kode){
        return $this->db->select('transaksi_item.id,transaksi.id_transaksi,transaksi.kode,transaksi.tgl_transaksi,akun.nama_reff,transaksi.no_reff,transaksi_item.jenis_saldo,transaksi_item.saldo,transaksi.tgl_input,transaksi_item.deskripsi,transaksi.memo')
                        ->from($this->table)
                        ->where('transaksi.kode',$kode)
                        ->join('transaksi_item','transaksi_item.kode = transaksi.kode')
                        ->join('akun','transaksi.no_reff = akun.no_reff')
                        ->order_by('tgl_transaksi','ASC')
                        ->order_by('tgl_input','ASC')
                        ->order_by('jenis_saldo','ASC')
                        ->get()
                        ->result();
    }

    public function getTotalSaldoDetail($jenis_saldo,$kode){
        return $this->db->select_sum('saldo')
                        ->from('transaksi_item')
                        ->where('transaksi_item.kode',$kode)
                        ->where('jenis_saldo',$jenis_saldo)
                        ->get()
                        ->row();
    }
    public function getTotalSaldoBy($where){
        return $this->db->select_sum('saldo')
                        ->from('transaksi_item')
                        ->where($where)
                        ->get()
                        ->row();
    }
    public function getDefaultValues(){
        return [
            'tgl_transaksi'=>date('Y-m-d'),
            'no_reff'=>'',
            'deskripsi[]'=>'',
            'memo'=>'',
            'jenis_saldo[]'=>'',
            'saldo[]'=>'',
            'kode'=>'',
        ];
    }

    public function getValidationRules(){
        return [
            [
                'field'=>'tgl_transaksi',
                'label'=>'Tanggal Transaksi',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'memo',
                'label'=>'Memo',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'no_reff',
                'label'=>'No Reff',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'deskripsi[]',
                'label'=>'Deskripsi Akun',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'jenis_saldo[]',
                'label'=>'Jenis Saldo',
                'rules'=>'trim|required'
            ],
            [
                'field'=>'saldo[]',
                'label'=>'Saldo',
                'rules'=>'trim|required|numeric'
            ],
            [
                'field'=>'kode',
                'label'=>'Kode Transaksi',
            ]
        ];
    }

    public function validate(){
        $rules = $this->getValidationRules();
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_error_delimiters('<span class="text-danger" style="font-size:14px">','</span>');
        return $this->form_validation->run();
    }
    public function countTransaksiNoReff($noReff){
        return $this->db->where('no_reff',$noReff)->get($this->table)->num_rows();
    }
    public function getNumTransaksi(){
        $this->db->from($this->table);
        $this->db->order_by('kode','desc');
        $result = $this->db->get();
        if($result->num_rows()==0){
            return "VKK"."001";
        }
        else{
            $data=$result->row_array();
            $kode = substr((string)$data['kode'], 3);
            $num_length = strlen($kode);
            $num = (int)$kode;
            $num = $num + 1;
            $kode='';
            $no_reff='';
            for ($i=1; $i<$num_length; $i++) { 
                # code...
                $no_reff = $no_reff.'0';
            }
            return "VKK".$no_reff.$num; 
        }
    }
}