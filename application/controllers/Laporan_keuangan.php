<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_keuangan extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->helper(['url','form','sia','tgl_indo']);
        $this->load->library(['session','form_validation']);
        $this->load->model('Jurnal_model','jurnal',true);
        $this->load->model('Penyesuaian_model','penyesuaian',true);
        $login = $this->session->userdata('login');
        if(!$login){
            redirect('login');
        }
        else{
            if($this->session->userdata('level')!='admin'){
               redirect('error_web/privilages_user');
            }
        }
    }
	public function index(){
		$titleTag = 'Laporan Keuangan';
        $content = 'laporan_keuangan/laporan_keuangan_main';
        $listJurnal = $this->jurnal->getJurnalByYearAndMonth();
        $tahun = $this->jurnal->getJurnalByYear();
        $this->load->view('template',compact('content','listJurnal','titleTag','tahun'));
	}
	public function laporanKeuanganDetail(){
        $content = 'laporan_keuangan/laporan_keuangan';
        $titleTag = 'Laporan Keuangan';

        $bulan = $this->input->post('bulan',true);
        $tahun = $this->input->post('tahun',true);
        $jurnals = null;

        if(empty($bulan) || empty($tahun)){
            redirect('laporan_keuangan');
        }

        $jurnals = $this->jurnal->getJurnalJoinAkunDetail($bulan,$tahun);
        $totalDebit = $this->jurnal->getTotalSaldoDetail('debit',$bulan,$tahun);
        $totalKredit = $this->jurnal->getTotalSaldoDetail('kredit',$bulan,$tahun);
        $saldo = $this->penyesuaian->getSaldo(['month(tgl_input)'=>$bulan,'year(tgl_input)'=>$tahun])->result();
        if($jurnals==null){
            $this->session->set_flashdata('dataNull','Data Jurnal Dengan Bulan '.bulan($bulan).' Pada Tahun '.date('Y',strtotime($tahun)).' Tidak Di Temukan');
            redirect('laporan_keuangan');
        }
        $this->load->view('template',compact('content','jurnals','totalDebit','totalKredit','titleTag','saldo'));
    }
}